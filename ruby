#! /bin/bash
#for Ruby and Apache,Passenger
yum remove ruby
yum -y install gcc zlib-devel openssl-devel
wget ftp://ftp.ruby-lang.org/pub/ruby/1.9/ruby-1.9.3-p484.tar.gz
tar zxvf ruby-1.9.3-p484.tar.gz
cd ruby-1.9.3-p484.tar.gz
./configure
make
make install
cd ~
PATH=$PATH:/usr/local/bin
export PATH
gem install passenger
passenger-install-apache2-module
